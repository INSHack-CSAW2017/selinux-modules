apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

echo "deb [arch=armhf] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
     $(lsb_release -cs) stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list

 apt-get update

 apt-get install docker-ce

 docker run armhf/hello-world

#####
git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLCBuilder.git
cd Docker_PLCBuilder/

cp /opt/openplc/core/glue_generator build/bin/
cp /opt/openplc/st_optimizer_src/st_optimizer build/bin/
cp /opt/openplc/matiec_src/iec2c build/bin/

 make build -B

make compile ST_FILE=program.st OUT=program || sudo make remove

#####
git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLC
cd Docker_PLC
make build -B

make start-plc PROGRAM=/root/Conteneurs/Docker_PLCBuilder/program

sudo make stop-plc


sudo apt-get update && sudo apt-get install python3 python3-pip
sudo pip3 install jsonrpcclient

python3 << EOF
import jsonrpcclient

#La vous aurez toutes les sorties du PLC
jsonrpcclient.request('http://localhost:8080/', 'get_bool_outputs')

#Ici vous settez le booleen 3 sur vrai
jsonrpcclient.request('http://localhost:8080/', 'set_bool_input', index=3,value=True)
EOF



#########
docker ps -a
docker rm ID
docker images
docker rmi ID
docker logs plc-container
 docker port plc-container
docker inspect plc-container


##
cd
cd selinux-modules/container-selinux/
make load

Dans /lib/systemd/system/docker.service, changer : 
ExecStart=/usr/bin/dockerd -H fd:// --selinux-enabled

systemctl daemon-reload
touch /.autorelabel
reboot







